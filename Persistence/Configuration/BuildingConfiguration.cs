﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configuration
{
    public class BuildingConfiguration : IEntityTypeConfiguration<Building>
    {
        public void Configure(EntityTypeBuilder<Building> builder)
        {
            builder.Property(m => m.Name).HasMaxLength(100).IsRequired();
            builder.Property(m => m.Num).HasMaxLength(100);
            builder.Property(m => m.Address).HasMaxLength(500);
            builder.HasQueryFilter(m => !m.Deleted);
            builder.HasOne(m => m.Person)
                .WithMany(m => m.Buildings).HasForeignKey(m => m.PersonId);
        }
    }
}
