﻿using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Entities;

namespace Domain.Interfaces
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
    }
}