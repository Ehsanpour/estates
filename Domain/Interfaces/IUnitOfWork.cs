﻿using System;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IPersonRepository Persons { get; }
        IBuildingRepository Buildings { get; }
        Task<int> Complete();
    }
}