﻿using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Entities;

namespace Domain.Interfaces
{
    public interface IBuildingRepository : IGenericRepository<Building>
    {
    }
}