﻿
namespace Domain.Entities
{
    public class Log : BaseEntity
    {
        public Action Action { get; set; }
        public string IpAddress { get; set; }
        public string Table { get; set; }
        public int RowId { get; set; }
    }

    public enum Action
    {
        Create = 5,
        Update = 10,
        Delete = 15
    }
}
