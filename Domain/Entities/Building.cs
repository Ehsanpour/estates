﻿
namespace Domain.Entities
{
   public class Building : BaseEntity
    {
        public string Name { get; set; }
        public float Area { get; set; }
        public string Num { get; set; }
        public string Address { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public bool North { get; set; }
        public bool Deleted { get; set; }
    }
}
