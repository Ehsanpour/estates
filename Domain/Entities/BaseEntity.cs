﻿using System;
using System.Globalization;
using System.Text.Json.Serialization;

namespace Domain.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        [JsonIgnore]
        public DateTime RegisterDate { get; set; }
        public string RegisterDateDisplay => RegisterDate.ToString(CultureInfo.CurrentCulture);
        public BaseEntity()
        {
            RegisterDate = DateTime.Now;
        }
    }
}
