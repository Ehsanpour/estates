﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Person : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Tell { get; set; }
        public virtual ICollection<Building> Buildings { get; set; }
    }
}
