﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Dtos
{
    public class BuildingDto : BaseDto
    {
        public string Name { get; set; }
        public float Area { get; set; }
        public string Num { get; set; }
        public string Address { get; set; }
        public int PersonId { get; set; }
        public bool North { get; set; }

        public Building ToEntityBuilding(BuildingDto dto, int? id = 0)
        {
            return new Building
            {
                Id = id ?? 0,
                Name = dto.Name,
                North = dto.North,
                Area = dto.Area,
                Address = dto.Address,
                Num = dto.Num,
                PersonId = dto.PersonId
            };
        }
    }
}
