﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Dtos
{
    public class PersonDto : BaseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Tell { get; set; }

        public Person ToEntityPerson(PersonDto personDto, int? id = 0)
        {
            var person = new Person
            {
                Id = id ?? 0,
                FirstName = personDto.FirstName,
                LastName = personDto.LastName,
                Tell = personDto.Tell
            };
            return person;
        }
    }
}
