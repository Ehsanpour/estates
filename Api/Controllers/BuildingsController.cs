﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuildingsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public BuildingsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet(Name = nameof(GetAllBuildings))]
        public async Task<IReadOnlyList<Building>> GetAllBuildings()
        {
            return await _unitOfWork.Buildings.ListAllAsync();
        }
        [HttpGet("{id:int}", Name = nameof(BuildingGetById))]
        public async Task<Building> BuildingGetById(int id)
        {
            return await _unitOfWork.Buildings.GetByIdAsync(id);
        }

        [HttpPost(Name = nameof(AddBuilding))]
        public async Task<int> AddBuilding([FromBody] BuildingDto building)
        {
            var model = building.ToEntityBuilding(building);
            await _unitOfWork.Buildings.Add(model);
            return await _unitOfWork.Complete();
        }
        [HttpPut("{id:int}", Name = nameof(UpdateBuilding))]
        public async Task<int> UpdateBuilding([FromBody] BuildingDto building, int id)
        {
            var model = building.ToEntityBuilding(building, id);
            await _unitOfWork.Buildings.Update(model);
            return await _unitOfWork.Complete();
        }
        [HttpDelete("{id:int}", Name = nameof(DeleteBuilding))]
        public async Task<int> DeleteBuilding(int id)
        {
            await _unitOfWork.Buildings.Delete(id);
            return await _unitOfWork.Complete();
        }
    }
}
