﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public PersonsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet(Name = nameof(GetAllPersons))]
        public async Task<IReadOnlyList<Person>> GetAllPersons()
        {
            return await _unitOfWork.Persons.ListAllAsync();
        }
        [HttpGet("{id:int}", Name = nameof(PersonGetById))]
        public async Task<Person> PersonGetById(int id)
        {
            return await _unitOfWork.Persons.GetByIdAsync(id);
        }

        [HttpPost(Name = nameof(AddPerson))]
        public async Task<int> AddPerson([FromBody] PersonDto person)
        {
            var model = person.ToEntityPerson(person);
            await _unitOfWork.Persons.Add(model);
            return await _unitOfWork.Complete();
        }
        [HttpPut("{id:int}", Name = nameof(UpdatePerson))]
        public async Task<int> UpdatePerson([FromBody] PersonDto person, int id)
        {
            var model = person.ToEntityPerson(person, id);
            await _unitOfWork.Persons.Update(model);
            return await _unitOfWork.Complete();
        }
        [HttpDelete("{id:int}", Name = nameof(DeletePerson))]
        public async Task<int> DeletePerson(int id)
        {
            await _unitOfWork.Persons.Delete(id);
            return await _unitOfWork.Complete();
        }
    }
}
