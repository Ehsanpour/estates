﻿using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Repositories
{
    public class BuildingRepository : GenericRepository<Building>, IBuildingRepository
    {
        public BuildingRepository(Context context, IHttpContextAccessor accessor) : base(context, accessor)
        {
        }
        public async Task Delete(Building model)
        {
            var entity = await GetByIdAsync(model.Id);
            if (entity != null)
            {
                entity.Deleted = true;
                Context.Entry(entity).State = EntityState.Modified;
                await Context.SaveChangesAsync();
            }
        }
    }
}
