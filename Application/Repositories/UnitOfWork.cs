﻿using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.AspNetCore.Http;
using Persistence;

namespace Application.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Context _context;
        public UnitOfWork(Context context, IHttpContextAccessor accessor)
        {
            _context = context;
            Persons = new PersonRepository(_context,accessor);
            Buildings = new BuildingRepository(_context, accessor);
        }
        public IPersonRepository Persons { get; private set; }
        public IBuildingRepository Buildings { get; private set; }
        public Task<int> Complete()
        {
            return _context.SaveChangesAsync();
        }
        public async void Dispose()
        {
           await _context.DisposeAsync();
        }
    }
}
