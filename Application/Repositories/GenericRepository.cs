﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Action = Domain.Entities.Action;

namespace Application.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly Context Context;
        private readonly IHttpContextAccessor _accessor;
        public GenericRepository(Context context, IHttpContextAccessor accessor)
        {
            Context = context;
            _accessor = accessor;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public async Task<IReadOnlyList<T>> ListAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> expression)
        {
            return await Context.Set<T>().Where(expression).ToListAsync();
        }

        public async Task Add(T model)
        {
            Context.Set<T>().Add(model);
            await Context.SaveChangesAsync();
            Context.Logs.Add(new Log
            {
                Action = Action.Create,
                IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress?.ToString(),
                Table = typeof(T).Name,
                RowId = model.Id
            });
        }

        public async Task Update(T model)
        {
            var entity = await GetByIdAsync(model.Id);
            Context.Entry(entity).State = EntityState.Modified;
            Context.Logs.Add(new Log
            {
                Action = Action.Update,
                IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress?.ToString(),
                Table = typeof(T).Name,
                RowId = entity.Id
            });
        }

        public async Task Delete(int id)
        {
            var entity = await GetByIdAsync(id);
            if (entity != null)
            {
                Context.Set<T>().Remove(entity);
                Context.Logs.Add(new Log
                {
                    Action = Action.Delete,
                    IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress?.ToString(),
                    Table = typeof(T).Name,
                    RowId = entity.Id
                });
            }
        }

        Task<IReadOnlyList<T>> IGenericRepository<T>.Find(Expression<Func<T, bool>> expression)
        {
            throw new NotImplementedException();
        }
    }
}
